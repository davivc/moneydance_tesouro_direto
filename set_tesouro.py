import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import json
from datetime import datetime

## BEGIN Functions
def setPriceForSecurity(symbol, price, dateint):
    price = 1/price
    security = currencies.getCurrencyByTickerSymbol(symbol)
    if not security:
      return
    if dateint:
        snapshot = security.getSnapshotForDate(dateint)
        if snapshot.getDateInt() == dateint:
            # print "Price for %s already defined" % (security)
            return
        security.setSnapshotInt(dateint, price).syncItem()
        security.setUserRate(price)
        security.syncItem()
        print "Successfully set price for %s at %s" % (security, dateint)

def getRatesForSecurity(symbol, security_type = 'titulos'):
    with requests.Session() as s:
        headers = {
            'X-Gravitee-Api-Key': API_KEY,
            'Content-Type': 'application/json'
        }
        # timeout to prevent blocks
        connect_timeout, read_timeout = 10.0, 30.0

        url = '{0}/tesouro-direto/{1}'.format(API_URL, symbol)
        if security_type == 'fundos':
            url = '{0}/fundos-investimento/{1}'.format(API_URL, symbol)
        try:
            titulo = s.get(url,
                headers=headers,
                timeout=(connect_timeout, read_timeout),
                verify=False)
            response = json.loads(titulo.content)
            if titulo.status_code != 200:
                print('[%s] %s' % (symbol, response['msg']))
                return []
            return response
        except Exception as e:
            print(e)
            return []
## END Functions


API_KEY = '7ecc7d9c-1151-4f3d-8b78-602e84e3e630'
API_URL = 'https://dev.api-financeira.com'

# tesouro = {
#     'Tesouro Selic': 'LFT',
#     'Tesouro Prefixado': 'LTN',
#     'Tesouro IPCA+ com Juros Semestrais':'NTNB',
#     'Tesouro IGPM+ com Juros Semestrais':'NTNC',
#     'Tesouro IPCA+': 'NTNP',
#     'Tesouro Prefixado com Juros Semestrais':'NTNF'
# }
tesouro_keys = [
    'LFT',
    'LTN',
    'NTNB',
    'NTNC',
    'NTNP',
    'NTNF'
]

book = moneydance.getCurrentAccountBook()
currencies = book.getCurrencies()

# Set prices for tesouro direto
print('---- Setting prices for Tesouro Direto')
for currency in currencies:
    ticker = currency.getTickerSymbol()
    prefix = ticker.split("_")
    if prefix[0] in tesouro_keys:
        rates = getRatesForSecurity(ticker)
        for rate in rates:
            if rate['pu_venda_manha'] <> '':
                value = rate['pu_venda_manha']
            elif rate['pu_compra_manha'] <> '':
                value = rate['pu_venda_manha']
            date = int(datetime.strptime(rate['data_base'], "%Y-%m-%d").strftime("%Y%m%d"))
            setPriceForSecurity(ticker, value, date)
            setPriceForSecurity(ticker, value, 0)
        print('Prices for titulo %s set' % ticker)

# Set prices for fundos
print('---- Setting prices for Brazilian Investiment funds')
for currency in currencies:
    ticker = currency.getTickerSymbol()
    prefix = ticker.split("_")
    if prefix[0] == 'FI':
        rates = getRatesForSecurity(prefix[1], 'fundos')
        for rate in rates:
            value = rate['valor_quota']
            date = int(datetime.strptime(rate['data_competencia'], "%Y-%m-%d").strftime("%Y%m%d"))
            setPriceForSecurity(ticker, value, date)
            setPriceForSecurity(ticker, value, 0)
        print('Prices for %s fund set' % ticker)
