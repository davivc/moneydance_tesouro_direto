import requests
from datetime import datetime

CSV_URL = 'https://www.tesourotransparente.gov.br/ckan/dataset/df56aa42-484a-4a59-8184-7676580c81e3/resource/796d2059-14e9-44e3-80c9-2d9e30b405c1/download/PrecoTaxaTesouroDireto.csv'

with requests.Session() as s:
    download = s.get(CSV_URL, verify=False)

    decoded_content = download.content.decode('utf-8')

    cr = csv.reader(decoded_content.splitlines(), delimiter=';')
    my_list = list(cr)

# Remove first row
values = my_list[1:]
# Sort by date
values.sort(key=lambda x:datetime.strptime(x[2], "%d/%m/%Y"))

tesouro = {
    'Tesouro Selic': 'LFT',
    'Tesouro Prefixado': 'LTN',
    'Tesouro IPCA+ com Juros Semestrais':'NTNB',
    'Tesouro IGPM+ com Juros Semestrais':'NTNC',
    'Tesouro IPCA+': 'NTNP',
    'Tesouro Prefixado com Juros Semestrais':'NTNF'
}

# book = moneydance.getCurrentAccountBook()
# currencies = book.getCurrencies()
def setPriceForSecurity(symbol, price, dateint):
  price = 1/price
  security = currencies.getCurrencyByTickerSymbol(symbol)
  if not security:
    return
  if dateint:
      snapshot = security.getSnapshotForDate(dateint)
      if snapshot.getDateInt() == dateint:
          print "Price for %s already defined" % (security)
          return
      security.setSnapshotInt(dateint, price).syncItem()
      security.setUserRate(price)
      security.syncItem()
      print "Successfully set price for %s" % (security)

for row in values:
    type = row[0]
    value = 0
    if row[7] <> '':
        value = float(row[7].replace(",", "."))
    elif row[6] <> '':
        value = float(row[6].replace(",", "."))
    if tesouro.get(type) and value > 0:
        year = datetime.strptime(row[1], "%d/%m/%Y").strftime("%Y")
        ticker = tesouro.get(type) + "_" + year
        date = int(datetime.strptime(row[2], "%d/%m/%Y").strftime("%Y%m%d"))
        setPriceForSecurity(ticker, value, date)
        setPriceForSecurity(ticker, value, 0)
