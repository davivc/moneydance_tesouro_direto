# Moneydance <> Tesouro Direto

This script updates historical prices for Brazilian government bonds (http://www.tesouro.fazenda.gov.br/tesouro-direto) on your Moneydance.

## Requirements

### Moneydance
https://moneydance.com/

### Ticker Symbol
You must configure your Tesouro Direto ticker symbols as **Prefix_YEAR** (eg. LTN_2017, LFT_2023, etc)

**Prefixes List**
* Tesouro Selic => 'LFT'
* Tesouro Prefixado => 'LTN'
* Tesouro IPCA+ com Juros Semestrais => 'NTNB'
* Tesouro IGPM+ com Juros Semestrais => 'NTNC'
* Tesouro IPCA+ => 'NTNP'
* Tesouro Prefixado com Juros Semestrais => 'NTNF'

### References

* The prices come from the official government data here: https://www.tesourotransparente.gov.br/ckan/dataset/df56aa42-484a-4a59-8184-7676580c81e3/resource/796d2059-14e9-44e3-80c9-2d9e30b405c1/download/PrecoTaxaTesouroDireto.csv.
* The metadata of this dataset are located here: https://www.tesourotransparente.gov.br/ckan/dataset/df56aa42-484a-4a59-8184-7676580c81e3/resource/1a8eb2e3-4902-4a38-a1eb-6410f23d90de/download/Taxa.pdf
* More information about the data here: http://www.tesourotransparente.gov.br/ckan/dataset/taxas-dos-titulos-ofertados-pelo-tesouro-direto
* The script uses the 7th column (PU Venda Manha) or the selling price, not the buying price. But you can change inside the script.
